package com.sqldbsamples;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat; 
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;


import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.DriverManager;

public class App {
	
	private static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private static Date date = new Date();
	
	
	public static void databaseToSpark(String tableName, String sql) {
		

	     // Connect to database
	        String url = "jdbc:sqlserver://devcloverleaf.database.windows.net:1433;database=CLF_DB;user=cloverleaf@devcloverleaf;password=Cl0verle@f!;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
	        Connection connection = null;
	        
	        FileWriter fileWriter = null;
	        
	        final String storageConnectionStringDestination =
				    "DefaultEndpointsProtocol=https;" +
				    "AccountName=cloverleafstoragespark;" +
				    "AccountKey=PENWlTyhPBOKErJ18hMxleVRXC+7X5pjQ0gEIBSddEBGSDW6riLk1uURtG9Re07yOGj9Eqs9EAp3WlxN/Wc7OQ==";

	        try {
	        	
	        	    //Delimiter used in CSV file
				    final String TAB_DELIMITER = "\t";
				    final String NEW_LINE_SEPARATOR = "\n";
	        	
	                connection = DriverManager.getConnection(url);
	                String schema = connection.getSchema();
	                System.out.println("Successful connection - Schema: " + schema);

	                System.out.println("Query data example:");
	                System.out.println("=========================================");

	                // Create and execute a SELECT SQL statement.
	                String selectSql = "SELECT * from "+tableName;
	                
	                
	                
	                try (Statement statement = connection.createStatement();
	                		
	                		
	                		
	                		
	                    ResultSet resultSet = statement.executeQuery(selectSql)) {

	                	
	        			
	        			try {
	        				fileWriter = new FileWriter(tableName+".txt");

	        			
	                        
	                        while (resultSet.next())
	                        {
	                            
	                            ResultSetMetaData rsmd=resultSet.getMetaData();
	                            int columnLength = rsmd.getColumnCount();
	                            
	                            for (int i = 1; i <= columnLength; i++) {
	                            	fileWriter.append(String.valueOf(resultSet.getString(i)));
	                            	if(i<columnLength) {
	                            		fileWriter.append(TAB_DELIMITER);
	                            	}
								}
	                            
                           
	        					fileWriter.append(NEW_LINE_SEPARATOR);
	        					                            
	                        }
	                        
	                        System.out.println(" records fetched "+tableName);
	        				
	        				
	        				System.out.println(tableName+" Txt file was created successfully !!!");
	        			} catch (Exception e) {
	        				System.out.println("Error in TxtFileWriter !!!");
	        				e.printStackTrace();
	        			} finally {
	        				
	        				try {
	        					fileWriter.flush();
	        					fileWriter.close();
	        					
	        					File sourceFile = new File(".\\"+tableName+".txt");
	        			        System.out.println("File Name : "+tableName+".txt");
	        			        
	        			    	CloudStorageAccount storageAccountDestination;
	        			    	CloudBlobClient blobClient = null;
	        			    	CloudBlobContainer container=null;
	        			        
	        			      
	        			  		  
	        			        	// Parse the connection string and create a blob client to interact with Blob storage
	        			        	storageAccountDestination = CloudStorageAccount.parse(storageConnectionStringDestination);
	        				    	blobClient = storageAccountDestination.createCloudBlobClient();
	        						container = blobClient.getContainerReference("sparkcloverleaf-2017-12-14t11-57-47-334z/AtlantaBeveregeJeanCoutu/"+tableName); //Blob name
	        				    	
	        			  			
	        			  		    
	        			  			//Getting a blob reference
	        			  			CloudBlockBlob blob = container.getBlockBlobReference(tableName+".txt");
	        			  		    
	        			  		    //Creating blob and uploading file to it
	        			  			System.out.println("Uploading the "+tableName+" Txt file ");
	        			  		    blob.uploadFromFile(sourceFile.getAbsolutePath());

	        			  		
	        			  		
	        			  		    
	        									
	        				}
	        		        catch (IOException e) {
	        					System.out.println("Error while flushing/closing fileWriter !!!");
	        	                e.printStackTrace();
	        				} 
	        				
	        				catch(StorageException ex) {
	        					System.out.println(String.format("Error returned from the service. Http code: %d and error code: %s", ex.getHttpStatusCode(), ex.getErrorCode()));
	        				}
	        				finally {
	        					//Assume that the arguments are in correct order    //FROM BLOB TO SPARK CLUSTER
	        				    String clusterName = "sparkcloverleaf";
	        				    String clusterAdmin = "admin";
	        				    String clusterPassword = "Cloverleaf@123";

	        				    //Variables to hold statements, connection, and results
	        				    Connection conn=null;
	        				    Statement stmt = null;
	        				    
	        				    ResultSet res1 = null;
	        				    

	        				    try
	        				    {
	        				      //Load the HiveServer2 JDBC driver
	        				      Class.forName("org.apache.hive.jdbc.HiveDriver");

	        				      //Create the connection string
	        				      // Note that HDInsight always uses the external port 443 for SSL secure
	        				      //  connections, and will direct it to the hiveserver2 from there
	        				      //  to the internal port 10001 that Hive is listening on.
	        				      String connectionQuery = String.format(
	        				        "jdbc:hive2://%s.azurehdinsight.net:443/default;transportMode=http;ssl=true;httpPath=/hive2",
	        				        clusterName);

	        				      //Get the connection using the cluster admin user and password
	        				      conn = DriverManager.getConnection(connectionQuery,clusterAdmin,clusterPassword);
	        				      stmt = conn.createStatement();

	        				      

	        				      //Drop the 'hivesampletablederived' table, if it exists
	        				      
	        				      stmt.execute("drop table if exists " + tableName);

	        				      
	        				      //Create Database in Cluster
	        				      stmt.execute("create database if not exists AtlantaBeveregeJeanCoutu");
	        				    
	        				      stmt.execute(sql);

	        				      //Retrieve and display a description of the table
	        				     // sql = "AtlantaBeveregeJeanCoutu." + tableName;
	        				      System.out.println("\nGetting a description of the table:");
	        				      System.out.println(sql);
	        				      res1 = stmt.executeQuery(sql);
	        				      while (res1.next()) {
	        				          System.out.println(res1.getString(1) + "\t" + res1.getString(2));
	        				      }

	        				     
	        				    }

	        				    //Catch exceptions
	        				    catch (SQLException e )
	        				    {
	        				      e.getMessage();
	        				      e.printStackTrace();
	        				      System.exit(1);
	        				    }
	        				    catch(Exception ex)
	        				    {
	        				      ex.getMessage();
	        				      ex.printStackTrace();
	        				      System.exit(1);
	        				    }
	        				    //Close connections
	        				    finally {
	        				      if (res1!=null) res1.close();
	        				     
	        				      if (stmt!=null) stmt.close();
	        				      
	        				    }

	            				
	            			}
	        			}   
	                 connection.close();
	                 
	                }                
	        }
	        catch (Exception e) {
	                e.printStackTrace();
	        }
	    }
	

 public static void main(String[] args) {
	 
	
	 
	 Map<String, String> tableNames = new HashMap<String, String>();
	
	 
/*	 tableNames.put("Emotion", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Emotion\r\n" + 
	      		"(\r\n" + 
	      		"    EventId string,\r\n" + 
	      		"    EmotionName string,\r\n" + 
	      		"    EmotionValue string,\r\n" + 
	      		"    Source string\r\n" + 
	      		")\r\n" + 
	      		"ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'" + 
	            "STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Emotion'");
	 
	 tableNames.put("Product", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Product\r\n" + 
	 		"(\r\n" + 
	 		"    EventId string,\r\n" + 
	 		"    InventoryTrackingNumber string,\r\n" + 
	 		"    InventoryTrackingNumberType string,\r\n" + 
	 		"    ProductName string,\r\n" + 
	 		"    Source string,\r\n" + 
	 		"    Brand string\r\n" + 
	 		")\r\n" + 
	 		"ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+ 
      		"STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Product'");     
	
	 tableNames.put("EventData", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.EventData\r\n" + 
			"(\r\n" + 
			"    EventId string,\r\n" + 
			"    AgeRange string,\r\n" + 
			"    CompanyType string,\r\n" + 
			"    ConnectorType string,\r\n" + 
			"    ContentDisplayEvent string,\r\n" + 
			"    Customer string,\r\n" + 
			"    Emotion string,\r\n" + 
			"    Expression string,\r\n" + 
			"    FaceQualityName string,\r\n" + 
			"    FaceQualityValue string,\r\n" + 
			"    Gender string,\r\n" + 
			"    Products string,\r\n" + 
			"    Race string,\r\n" + 
			"    Region string,\r\n" + 
			"    SensorRegistry string,\r\n" + 
			"    SensorType string,\r\n" + 
			"    State string,\r\n" + 
			"    StockLevel string,\r\n" + 
			"    Unit string,\r\n" + 
			"    UnitType string,\r\n" + 
			"    Source string\r\n" + 
			")\r\n" + 
			"ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+
			"STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/EventData'");                  
	
	tableNames.put("Expression", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Expression\r\n" + 
			"(\r\n" + 
			"    EventId string,\r\n" + 
			"    ExpressionName string,\r\n" + 
			"    ExpressionValue string,\r\n" + 
			"    Source string   \r\n" + 
			")\r\n" + 
			"ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+
			"STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Expression'");        */
	
		/*tableNames.put("ShopperData", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.ShopperData\r\n" + 
			"(\r\n" + 
			"    EventId string,\r\n" + 
			"    Company string,\r\n" + 
			"    CompanyType string,\r\n" + 
			"    Connectortype string,\r\n" + 
			"    SensorRegistry string,\r\n" + 
			"    SensorType string,\r\n" + 
			"    EngagedShoppersCount string,\r\n" + 
			"    ShoppersCount string,\r\n" + 
			"    StoreName string,\r\n" + 
			"    StoreProvince string,\r\n" + 
			"    UnitName string,\r\n" + 
			"    UnitType string,\r\n" + 
			"    Source string   \r\n" + 
			")\r\n" + 
			"ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+
			"STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/ShopperData'");  */           
	 
	/*tableNames.put("ShopperTrackingData", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.ShopperTrackingData\r\n" + 
			"(\r\n" + 
			"    PartitionKey string,\r\n" + 
			"    RowKey string,\r\n" + 
			"    SensorRegistryId string,\r\n" + 
			"    EventId string,\r\n" + 
			"    Created string,\r\n" + 
			"    Value string,\r\n" + 
			"    Modified string,\r\n" + 
			"    Timestamp date\r\n" +  
			")\r\n" + 
			"ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+
			"STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/ShopperTrackingData'");      */
	
	tableNames.put("Sales_YTD", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Sales_YTD\r\n" + 
			"(\r\n" + 
			"    RetailAccounts string,\r\n" + 
			"    ADDR string,\r\n" + 
			"    CITY string,\r\n" + 
			"    WeekNumber string,\r\n" + 
			"    CaseEquivsAmount float,\r\n" + 
			"    Brand string,\r\n" + 
			"    year int,\r\n" + 
			"    ACCT int \r\n" + 
			")\r\n" + 
			"ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+
			"STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Sales_YTD'");                  
	
	
	tableNames.put("Stores_Sales", "CREATE EXTERNAL TABLE if not exists AtlantaBeveregeJeanCoutu.Stores_Sales\r\n" + 
			"(\r\n" + 
			"    StoreGroup string,\r\n" + 
			"    SaleType string,\r\n" + 
			"    WeekNumber int,\r\n" + 
			"    CalenderYear int,\r\n" + 
			"    Sales int \r\n" + 
			")\r\n" + 
			"ROW FORMAT DELIMITED FIELDS TERMINATED BY '\\t' lines terminated by '\\n'"+
			"STORED AS TEXTFILE LOCATION 'wasb:///AtlantaBeveregeJeanCoutu/Stores_Sales'"); 
	
	 
	 
	 for(Map.Entry<String, String> m : tableNames.entrySet()) {
		 System.out.println(m.getKey()+" _____ "+m.getValue());
		databaseToSpark(m.getKey(), m.getValue());
	 }

	 
 }

}